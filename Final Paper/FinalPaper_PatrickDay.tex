\documentclass[
	%a4paper, % Use A4 paper size
	letterpaper, % Use US letter paper size
]{jdf}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{filecontents,pdfpages}
\usepackage{listings}
\usepackage{color}
\usepackage[]{inputenc}
\usepackage{upquote}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{code-style}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,
    upquote=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space},
    frame=single
}


\addbibresource{references.bib}

\author{Patrick Day}
\email{Patrick.Day@gatech.edu}
\title{Final Paper}

\begin{document}
%\lsstyle

\maketitle

\section{Introduction}
In the 20 years, research in the field of Artificial Intelligence (AI) has regained popularity and has given rise to other areas of interest. The use of AI in education and other learning environments is not a new concept; however, the resurgence has led to explosive growth in Intelligent Tutoring Systems (ITS) and other knowledge-based systems. The purpose of an ITS is to provide computer-aided instruction while adapting to the knowledge deficiencies of the student using the system. A. Naser et al. provides a formal definition in (\cite{support-case-base}) as: ``[a]n Intelligent Tutoring System is a software that aims to provide immediate and customized instruction or feedback to learners, typically without interference from a human teacher.  An ITS has the general aim to facilitate learning in an evocative and efficient way by using a diversity of computing technologies." In Figure~\ref{fig:its-arch}, a typical architecture of a Intelligent Tutoring System is depicted as it is described in both (\cite{ooa-tutor}) and (\cite{its-survey}). There are four major components that compose the ITS which are: (1) the Student Model, which represents the students current knowledge state; (2) the Tutoring Model comprises instructional measures which are dependent on the content of the Student Model; (3) the Domain Model contains the domain knowledge about the content to be learned; and (4) the User Interface, the main communication between the User and the ITS.   

\begin{figure}[h]
	\centering
	\includegraphics[height=\maxheight, width=\maxwidth]{images/its-architecture.png}
	\caption{Intelligent Tutoring System Architecture}
	\label{fig:its-arch}
\end{figure}

The literature, specifically (\cite{ooa-tutor}) and (\cite{its-survey}) highlighted two major challenges as it pertains to intelligent tutoring systems. J. Bonar et al. in (\cite{ooa-tutor}), and D. R. Peachy~(\cite{plan-tutor}) both describe architectures of Intelligent Tutoring Systems with fixed data repositories. The system intelligence is derived in how it adapts to the learner. This limitation leads to ITS' being narrowly focused. The authors of (\cite{its-survey}) surveyed 68 existing intelligent tutoring systems found in literature over an 18 year period. The one challenge each system had to grapple with was how to accurately model the student's current understand of the material being taught. There were three overall strategies that evolved; (1) the overlay model tries to compare the student's behavior to an expert; (2) the perturbation model adds noise to account for possible misconceptions in understanding the domain; and (3) the learning base model focuses on the process of knowledge acquisition, because misconceptions are developed during that process. Both of the mentioned issues are related to the lack of ``common sense" or cognitive behavior. 

\begin{figure}[h]
	\centering
	\includegraphics[height=\maxheight, width=\maxwidth, trim={0 50 0 100}, clip]{images/its_model.png}
	\caption{Intelligent Tutoring Systems with Meta-Models}
	\label{fig:its-model}
\end{figure}

The purpose of this research is to create an intelligent tutoring system with the ability to acquire new knowledge, and evaluate the effectiveness of that system. The plan is to modify a ITS built for network intrusion detection (\cite{id-its}), which is used to teach network administrators how to effectively detect and counter network intrusion. The goal is to transform the original architecture, specifically the Student, Tutoring, and Domain models into meta-models, shown in Figure~\ref{fig:its-model}. The decision to use a meta-models as surrogates was made in hopes of simplifying the resulting system, and removing some complexity. The ability of the meta-model to handle multi-modal data will be crucial, therefore, at two experiments are planned to test the validity of this approach, and adjustments could be made depending on the results. 
%The experiments are as follows:
%		
%		\begin{enumerate}
%  			\item Transfer knowledge learned on a similar computer system, but different network (this represents multi-domain data)
%  			\item Transfer knowledge created from different modalities (this represents multi-modal data) 
%		\end{enumerate}

%Once the meta-model has been integrated, more experimentation will be required. However, due to the time constraints of the course, this may be a discussion for future work. This research will be quantitative in nature and use empirical analysis wherever possible.	The overall goal is to show that intelligent tutoring systems that both incorporate: (1) multi-modal \& multi-domain data; and (2) a learning feedback loop to acquire new knowledge are more effective than standard I

\section{Background}
Cognitive or Intelligent Systems behavior is derived from both the knowledge-base and the system architecture. J. Laird, the creator of the SOAR architecture~(\cite{soar}) said ``if the architecture remains static then changing the knowledge will also change the behavior". SOAR is a general purpose architecture for developing intelligence systems; which is based on Production Systems. Production systems use ``If-Then" rules, shown in Figure~\ref{fig:rules-soar}a to define behavior, and typically have both a long-term and short-term or working memory. Figure~\ref{fig:rules-soar}b shows the Soar architecture. The long term memory can be further segmented into Procedural, Semantic, and Episodic regions also referred to as “Metacognition”. Each region holds different types of knowledge and representations allowing the intelligent agent to perform reasoning about the information in the Working Memory. 

\begin{figure}[h]
	\centering
    \includegraphics{images/rules-soar.png}
    \caption{(a) Example production rules; and (b) The SOAR Architecture as explained by J. Laird~(\cite{soar})}
    \label{fig:rules-soar}    
\end{figure}

\subsubsection{Working Memory - Frames}
Frames are another form of knowledge representation and serves as a basis for Common Sense Reasoning. What makes Frames so powerful is the ability to derive or infer meaning from natural language. In comparison with Production Rules which are a set of “if-then” patterns Frames are of a more detailed nature. Frames consists of fields referred to as “slots” and values called “fillers” which is not too different from an object in Object Oriented Programming, the slots and fillers representing the object’s state. In many cognitive architectures Production Rules and Frames work in tandem with each other with the former being used as long-term and the latter as short-term or working memory.

\subsubsection{Long Term Memory - Procedural, Semantic, Episodic}
\textbf{Procedural Memory:}
As discussed in the previous section the long term memory is composed of Procedural, Semantic, and Episodic knowledge representations. The Procedural representation take the form of Production Rules, a set of If-Then statements. Production Rules can be chained together to provide both deductive and inductive reasoning as shown in Figure~\ref{fig:rules-soar}a.

\textbf{Semantic Memory:}
Semantic Memory is one of two types of declarative or explicit memory which is made up of general knowledge. Knowledge in this segment is meant to be the accumulation of learned information, and therefore can be described with any type of knowledge representation. The Semantic Memory will be bootstrapped with functional, nonfunctional, and system requirements with domain knowledge.

\textbf{Episodic Memory:}
The other type of explicit memory is Episodic Memory encompasses the experience and specific events. Case Based Reasoning (CBR) is typically used to implement Episodic memory because of dealing with the experience aspect. Therefore as described by CBR the process of Retrieval, Adaptation, Evaluation, and Storage. This memory segment is the perfect for reusable data representations, and must incorporate a feedback learning loop.

The architecture presented in (\cite{ooa-tutor}) closely resembles what is described in (\cite{ibm-auto-comp}) for autonomic computing with the obvious exception of the knowledge feedback loop. Autonomic computing draws inspiration from the Autonomic Nervous System (ANS) which is the system in the human body that controls the breathing, heartbeat, temperature regulation, and any other action that does not require conscience thought. Therefore, autonomic computing are software systems that are self-configuring, self-healing, and self-aware; systems of these types are often referred to as ``self-*" systems, where the ``*" is substituted for any learned behavior. The reference architecture for autonomic computing first introduced in~(\cite{ibm-auto-comp}) discuss the MAPE-K feedback loop referred to in Figure~\ref{fig:mape-k}. The MAPE-K loop is implemented by the ``Autonomic Manager" with manages the underlying system or artifact. In the `Monitor' phase, data is collected from sensors and evaluated by the manager and passed on to the `Analyze' phase. The task of the `Analyze' phase is to identify the current state of the system, which is then fed into the `Plan' phase. The goal of the `Plan' phase is too map a response that will drive the artifact towards the desired state. Specific effectors on the artifact are in charge of applying the computed response, thereby implementing the `Execute' phase of the MAPE-K loop. Finally, the `Knowledge' layer serves as an interface between the artifact and manager which, during regular operation and over time, acquires insights about the artifact. In general the research area of autonomic computing overlap with areas of meta-cognition and cognitive systems, the major similarity is the need for a knowledge-base and therefore, knowledge representation.

\begin{figure}[h]
	\centering
	\includegraphics[height=\maxheight, width=\maxwidth, trim={0 380 0 100}, clip]{images/mape-k.jpg}
	\caption{MAPE-K Feedback Loop.}
	\label{fig:mape-k}
\end{figure}

The research area of machine learning, more specifically, deep learning has a rich depth of literature using both `representational' \& `transfer' learning in a number of applications. The true strength of representational learning is that regardless of how it is obtained, once learned the knowledge can be reused~(\cite{represent-learning}); which leads directly into transfer learning. Transfer learning focuses on using pre-trained hidden layers to reduce the amount of time and data needed to train a Deep Neural Network (DNN)~(\cite{transfer-survey}). J. Lu et al. provided a framework to utilize previously-acquired knowledge to solve new but similar problems much more quickly and effectively~(\cite{transfer-int}). X. Jia et al.~(\cite{physics-ml}), for example, presented a number of ways physics could be used to constrain the learning of machine learning models. Typically there are three approaches to this method; (1) using physics models to simulate input data; (2) using physics models to generate initial weight values for neural networks; and (3) using physics models to regularize the learning function. Overall, this physics guided machine learning can be equated to transfer learning. Similarly, (\cite{meta-learn}), emulate properties of representation and transfer learning. The authors introduced a meta-learning method that could be combined with other model representations by reusing knowledge from previously learned tasks. R. M. Issaacson et al. described Metacognitive Knowledge Monitoring (MKM)as the ability to ``self-regulate" one's learning~(\cite{metacog}). Issaacson said that ``self-regulated learners are skillful at monitoring their learning and comprehension which as direct effect on each step in the self-regulation process." This cognitive behavior in students is exactly what the MAPE-K feedback loop in autonomic systems are designed to do; to be ``active, goal-directed" in achieving the specified learning task. The metaphorical threads of \textit{knowledge representation} and \textit{autonomic computing} weave together into a single idea, which is a ``general-purpose" machine learning model. 

Creating a general-purpose machine learning model is difficult. The difficulty lies in the fact that the learning objective is too broad, needing a high-order function to successfully train the model. Therefore, the higher the degree of the function, the more training data is required. Simply, the broader the learning objective, the more data that is needed. The majority of the research in the area of ``general-purpose machine learning" focuses on implementing ML as part of a larger framework or optimization~(\cite{generic-obj-rec}), (\cite{gp-ml-framework}), (\cite{meta-opt}). However, the literature shows that there have been attempts to create such generic machine learning models (e.g.,~\cite{generic-tree-ensemble}; \cite{comp-generic-ml}; \cite{gml}) and, in each case, the authors are relying on ensemble methods, to create a general-purpose machine learning model or ``meta-model". Ensemble learners utilize the prediction of multiple machine learning model instances and combine them using a function~(\cite{ensemble-methods}).

Ensemble methods are learning algorithms that construct a set of classifiers and classify new data points by taking a weighted vote of their predictions. There are several approaches to ensemble learning, including: Boosting~(\cite{boosting}), (\cite{exp-boost}), Bagging~(\cite{bagging-predict}), Stacked Generalization~(\cite{stackd-gen}), and Bayesian averaging, which is regarded as the original ensemble method~(\cite{ensemble-methods}); these algorithms are also referred to as ``meta-algorithms". The literature agrees that monolithic models generally suffer from three types of problems: (1) the statistical problem; (2) the computational problem; and (3) the representational problem~(\cite{ensemble-learn}), (\cite{ensemble-methods}), (\cite{ensemble-net-sec}). The statistical problem occurs when the training data is too small in regards to the model's complexity; the more degrees of freedom the model has, the more data is required to properly train it. The second problem, computational, refers to how an algorithm can get trapped in a local optimum while searching through the state space. The last problem is what leads difficulties in applying machine learning to compound learning objectives: the target function may not be represented in the search space. Ensemble methods address all of these issues, which is why they tend to provide better performance. Thomas Dietterich in~(\cite{ensemble-learn}) outlined five methods for constructing ensemble learners: Bayesian voting, manipulating training data, manipulating input features, manipulating output targets, and injecting randomness. From the listed ensemble methods, Stacked Generalization, first introduced by David H. Wolpert~(\cite{stackd-gen}), is of particular interest; because it is a way of combining models that have been previously learned.

\begin{lstlisting}[language=Python, style=code-style, caption=Auto-Encoder, label=auto-encoder, float, floatplacement=h!]
  from keras.models import Sequential, load_model
  from keras.layers import Dense, Dropout, LSTM, RepeatVector, Flatten
  
  steps = 2
    
  # Encoding Section
    unsw_model = Sequential()
    unsw_model.add(LSTM(20, activation='relu', input_shape=(step, 222), name='unsw_encoder', unroll=True))

    # Decoding Section
    unsw_model.add(RepeatVector(step))
    unsw_model.add(LSTM(20, activation='relu', return_sequences=True, name='unsw_decoder', unroll=True))
    unsw_model.add(TimeDistributed(Dense(222)))
    unsw_model.compile(optimizer=Adam(lr=5e-4, beta_1=0.9, beta_2=0.999), loss='mse')
    # unsw_model.layers[0].set_weights(candidate_weights)
else:
    # load the checkpoint from disk
    print("[INFO] loading {}...".format(args["model"]))
    unsw_model = load_model(args["model"])
    # update the learning rate
    print("[INFO] old learning rate: {}".format(K.get_value(unsw_model.optimizer.lr)))
    K.set_value(unsw_model.optimizer.lr, 5e-5)
    print("[INFO] new learning rate: {}".format(K.get_value(unsw_model.optimizer.lr)))

unsw_model.summary()
\end{lstlisting} 

\section{Project Discussion}
The purpose of this research is to create an intelligent tutoring system with the ability to acquire new knowledge, and evaluate the effectiveness of that system. The plan is to modify a ITS built for network intrusion detection~(\cite{id-its}), which is used to teach network administrators how to effectively detect and counter network intrusion. The goal is to transform the original architecture, specifically the Student, Tutoring, and Domain models into meta-models, shown in Figure~\ref{fig:its-model}. The decision to use a meta-models as surrogates was made in hopes of simplifying the resulting system, and removing some complexity. The ability of the meta-model to handle multi-modal data will be crucial, therefore, at two experiments are planned to test the validity of this approach, and adjustments could be made depending on the results. The experiments are as follows:
		
		\begin{enumerate}
  			\item Transfer knowledge learned on a similar computer system, but different network (this represents multi-domain data)
  			\item Transfer knowledge created from different modalities (this represents multi-modal data) 
		\end{enumerate}

Once the meta-model has been integrated, more experimentation will be required. However, due to the time constraints of the course, this may be a discussion for future work. The research was to be quantitative in nature and use empirical analysis wherever possible.	The overall goal was to show that intelligent tutoring systems that both incorporate: (1) multi-modal \& multi-domain data; and (2) a learning feedback loop to acquire new knowledge are more effective than standard ITS implementations.

This project started off well, with learning the Python Keras framework for deep learning, and successfully created a stacked encoder, using the specifications from the "AutoIDS: Auto-encoder Based Method for Intrusion Detection System"~(\cite{auto-ids}) using the UNSW-B15 dataset~(\cite{unsw-nb15}}) dataset. However, things took a turn for the worst, when it come implementing an Intelligent Tutoring System using a reference code base~(\cite{github-repo}). The goal was to stand-up a working version of the ITS to use as a baseline for empirical analysis. Once the ITS was running, one of the models would be swapped out for a meta-model, where a comparison could be made. The task of creating a working ITS was never achieved, which essentially threw off the entire timeline; with three full weeks dedicated to this effort.

At this point, the project's focus and scoped was redirected in hopes of getting useful results. The next step was to take the meta-model created, and try to integrate into the ITS reference code base. This attempt was also a failure, due to the fact that each model of the ITS, although independent still work in a synergetic fashion. So, even though the meta-model was indeed integrated none of the results were reliable to use. The time taken to getting this task completed was significant, which resulted in an incomplete project. However, the belief is with more time this issue could have been resolved.

\begin{figure}[h]
        \centering
        \includegraphics[trim={0 60 20 20},clip]{images/auto_encoder_loss.png}
        \caption{Comparison of the accuracy of two models, respectively with and without previous knowledge, representing System B}
        \label{loss}
    \end{figure}

In general, the issues that has plagued this project stems from the lack of depth with Intelligent Tutoring Systems. The deeper understanding would have netted better results, as is evident with successfully integrating the meta-model into the reference code. The research done up to this point has laid the ground work to identify gaps, justify a concept, and the construction of a methodology.

\subsection{Meta-Model Results}
This section presents the validation results for the meta-model constructing a stacked auto-encoder outlined in~(\cite{auto-ids}). The presented a semi-supervised approach, which uses the auto-encoder to learn the features of the dataset, and then transfers the representation to a meta-model. The meta-model is a classifier, using the same dataset as input to identify labeled samples. For all of the validation in section, we rely on the UNSW-B15 dataset~(\cite{unsw-nb15}), created and benchmarked by N. Moustafa et al.~(\cite{unsw-data}). The content of UNSW-NB15 includes a number of raw source files, such as PCAP, BRO, and Argus files. However, only the following files are relevant to this experiment:

	\begin{enumerate}
   		\item \texttt{UNSW-NB15\_GT.csv} \textemdash Ground Truth table
  	  	\item \texttt{UNSW\_NB15\_training-set.csv} \textemdash Training data
  		\item \texttt{UNSW\_NB15\_testing-set.csv} \textemdash Validation data
	\end{enumerate}
	
The PCAP data is ran through the auto-encoder in Listing~\ref{auto-encoder} to create a representation of the features by compressing them into a smaller dimension space, while maintaining the variance of the data. The ``Loss" represents the information loss during this process, which is shown in Figure~\ref{loss}. The learned data is then transferred to classifier and trained on the same dataset. For an accurate comparison another classifier was created trained on the dataset normally without any transferred knowledge. The accuracy of the two models are illustrated in Figure~\ref{comparison}.
	
    
    \begin{figure}[h]
 		\centering   
        \includegraphics[trim={0 60 20 20},clip]{images/compare.png}
        \label{comparison}
        \caption{Comparison between Auto-encoder vs. Classifier}   
    \end{figure}


\section{Conclusion}
This project was extremely interesting and rewarding, although, it ended with less than expected results. In retrospect, the scope really ambitious for the time frame for this course, and given more time to complete and deeper study of ITS a better methodology and approach could be found. However, learning how to conduct individual research was indeed worthwhile.


% \section{Experimental Results}
%    \label{sec:exp}
%    
%    In this section, we present the results of three experiments, aimed respectively at: (i) validating the benefits of re-using trained K-structures, as opposed to training a new model; (ii) showing the improvement provided by the newly introduced methodology, which unfreezes the weights among the layers of the neural network of the meta-model; (iii) demonstrating the need of our approach for a reduced amount of data, with respect to ... 
%    
%    For all of them, we rely on two public datasets, representing the network traffic of two unrelated Web Servers. Hereinafter, we will refer to these two Web Servers as \textit{System A} and \textit{System B}. The decision to use these datasets based on network traffic data was made due to their public availability, and due to the need for well-scoped multi-modal data. The use of separate servers on different networks ensures that the experiments will be run on different, however related domains.
    
    
%    
%    
%    The CRATE dataset~\cite{foi-iwlab-data}, produced by FOI's Information Warfare Laboratory, was used to simulate \textit{System A}; UNSW-B15, created and benchmarked by N. Moustafa et al.~\cite{unsw-data, unsw-phd}, was instead be used for \textit{System B}. 
%    
%    
%    \subsection{Modeling Similar Systems}
%    \label{exp1}
%    The purpose of this experiment is to show the benefits of re-using prior knowledge from a different domain when training a new model.
%    %demonstrate the creation of a generalized meta-model using K-structures from different domains.
%    In order to achieve this, 
%
%    Each dataset was then used to create a K-structure using an autoencoder, and then transferred to a new meta-model as described in Figure~\ref{meta-model}. It is important to note that we kept the weights for both K-structures frozen for this experiment, in order to maintain continuity with our previous work. The assembled meta-model was then trained and validated using ``Training" and ``Validation" datasets from \textit{System B}, respectively. The resulting meta-model, referred to as ``With-Prior Knowledge", is compared to a model trained from scratch with data from \textit{System B}. The new model, referred to as ``Without-Prior Knowledge", does include any K-structures, but have the same number of hidden layers.
%    
%    Figure~\ref{exp1_results} shows a comparison of the accuracy between the ``With Prior Knowledge" and the ``Without-Prior Knowledge" models, with respect to the number of epochs needed for training. Although both systems approach 85\% accuracy after $1600$ epochs, the ``With-Prior Knowledge" exhibited a faster convergence speed.
%    
%    The observed behavior is attributed to transfer learning, where pre-trained hidden layers have shown improved performance and a lower training time.  
%    
%    \subsection{Interaction Between K-structures}
%    \label{exp2}
%    In our previous work, the weights of each K-structure were frozen after encoding the respective dataset. As a consequence, once stacked and transferred into the meta-model, the correlation among the underlying datasets could not be properly identified. Nonetheless, the model was still able to provide reasonable performance because the training data used for the meta-model was a super-set representative of all the data encoded in the K-structures.
%    
%    However, with the advent of re-usability, there will eventually be a need to combine structures from completely different areas of concern. In this eventuality,the ability to learn the correlations among K-structures will play a key role in increasing the performance of the model. Therefore, the purpose of this experiment is to examine benefits of the newly introduced methodology, which allows us to re-use K-structures while keeping their weights unfrozen during the training process. To begin, a meta-model is constructed using K-structures created using the data from \textit{System B}. The new meta-model is trained under two separate scenarios in: (1) using frozen weights, leaving them untrainable, and (2) using unfrozen weights, allowing them to train.   
%    
%    \begin{figure}[h]
%        \centering
%        \includegraphics[trim={0 60 20 20},clip]{images/exp2_weight_comp.png}
%        \caption{Comparison between Frozen vs Unfrozen K-structure Weights}
%        \label{exp2_results}    
%    \end{figure}
%    
%    Figure~\ref{exp2_results} depicts the accuracy of the meta-model after training in both scenarios. In the scenario where the weights of the K-structures are unfrozen, the model achieved higher performance, reaching 89\% accuracy compared to the 81\% delivered by the frozen weight variant.
%    
%    %In Experiment~\ref{exp2}, we saw the interaction between K-structures, and how they could work together toward a learning objective. This experiment was a natural extension of the one conducted in the previous work, by unfreezing the training weights, we were able to see improved accuracy of the meta-model.
%    %This result gives credence to the possibility that K-structures can in fact be used as libraries, and as long as (1) the K-structure is relevant to the problem, and (2) the training weights are trainable; the likelihood the meta-model will converge to a solution is increased.
%    
%   \subsection{Comparison of Training Data Requirements}\label{exp3}
%    The combination of using K-Structures and a meta-model has been likened to a stacked generalized meta-learner, therefore, the purpose of this experiment is to reveal just how realistic that comparison happens to be. We will be using the data from \textit{System B}, so that we can compare the output of the stacked generalizer with the meta-model with unfrozen weights obtained from the Experiment~\ref{exp2}.
%    
%     \begin{figure}[h]
%        \centering
%        \includegraphics[trim={0 80 100 95},clip]{images/exp3_stacked_gen.png}
%        \caption{Comparison of Training Data Requirements}
%        \label{stacked_gen}    
%    \end{figure}
%    
%    \begin{figure}[h]
%        \centering
%        \includegraphics[trim={0 60 20 20},clip]{images/exp3_training_data_comp.png}
%        \caption{Comparison of Training Data Requirements}
%        \label{exp3_results}    
%    \end{figure}
%    
%    In order to create the stacked generalizer, we will replicate the setup of the experiment conducted in~\cite{stack-gen-when}, with one minor change. K.M. Ting et al, used three tier-0 classifiers C4.5, Naive Bayes (NB), and IB1; for the purpose of this demonstration only two are necessary. Therefore, we will be using C4.5 and NB as the tier-0 classifiers, and a linear regression neural network classifier for tier-1. Figure~\ref{stacked_gen}, illustrates the setup for this experiment; and as shown, we will be using the class probabilities instead of the assigned labels. Class probabilities are used so that predictions can be successfully ``stacked" and fed into the tier-1 linear regression model.
%         
%    Knowledge-structures are essentially pre-trained hidden layers of a deep neural network, and a major benefit of transfer learning is that the amount of data needed to train a meta-model is reduced. The purpose of the experiment is to investigate how the accuracy of the model varies according to the amount of training data that is available. To start, both the training and validation data from \textit{System B} are divided into three additional datasets, respectively, with only a percentage of the original data, represented by the following ratios: 25, 50, and 75. Including the original dataset, we now have four datasets ranging from 25\% to 100\% of the data. We then reuse the meta-model from the experiment described in Section~\ref{exp2}, in which the K-structure weight values remain trainable, and then create four instances of this model. The models are labeled as so: (1) the model trained on 25\% of the data, ``a\_25"; (2) trained on 50\% of data, ``b\_50"; (3) trained on 75\% of data, ``c\_75"; and (4) trained on all of the data, ``d\_100".  The output curves of each model are then compared to the output curve of the stacked generalizer shown in Figure~\ref{exp3_results}.
%    
%    The Figure illustrates that the model ``d\_100" is nearly identical to ``stacked gen" in both convergence speed and accuracy. The accuracy of ``stacked gen" reached 86\% compared to ``d\_100" with an accuracy 84\%, with nearly a 2\% improvement. However, the truly interesting result is the model labeled ``c\_75" because although it uses 25\% less training data the meta-model still achieved an accuracy of 75\%; this result gives credence that, by using K-structures, less training data is required to fine-tune the meta-model, when compared to a model that does not use K-structures, for the same number of epochs. In retrospect, this outcome is not surprising, as a major advantage of employing transfer learning is that whenever knowledge is ``transferred", the receiving model does not need as much training data.

\section{References}
\printbibliography[heading=none]

%\includepdf[pages=1,pagecommand={}]{TaskList.pdf}

\end{document}