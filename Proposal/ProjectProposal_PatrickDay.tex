\documentclass[
	%a4paper, % Use A4 paper size
	letterpaper, % Use US letter paper size
]{jdf}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{filecontents,pdfpages}

\addbibresource{references.bib}

\author{Patrick Day}
\email{Patrick.Day@gatech.edu}
\title{Project Proposal}

\begin{document}
%\lsstyle

\maketitle

\section{Introduction}
In the 20 years, research in the field of Artificial Intelligence (AI) has regained popularity and has given rise to other areas of interest. The use of AI in education and other learning environments is not a new concept; however, the resurgence has led to explosive growth in Intelligent Tutoring Systems (ITS) and other knowledge-based systems. The purpose of an ITS is to provide computer-aided instruction while adapting to the knowledge deficiencies of the student using the system. A. Naser et al. provides a formal definition in (\cite{support-case-base}) as: ``[a]n Intelligent Tutoring System is a software that aims to provide immediate and customized instruction or feedback to learners, typically without interference from a human teacher.  An ITS has the general aim to facilitate learning in an evocative and efficient way by using a diversity of computing technologies." In Figure~\ref{fig:its-arch}, a typical architecture of a Intelligent Tutoring System is depicted as it is described in both (\cite{ooa-tutor}) and (\cite{its-survey}). There are four major components that compose the ITS which are: (1) the Student Model, which represents the students current knowledge state; (2) the Tutoring Model comprises instructional measures which are dependent on the content of the Student Model; (3) the Domain Model contains the domain knowledge about the content to be learned; and (4) the User Interface, the main communication between the User and the ITS.   


\begin{figure}[h]
	\centering
	\includegraphics[height=\maxheight, width=\maxwidth]{images/its-architecture.png}
	\caption{Intelligent Tutoring System Architecture}
	\label{fig:its-arch}
\end{figure}

\subsection{The Problem}
The literature, specifically (\cite{ooa-tutor}) and (\cite{its-survey}) highlighted two major challenges as it pertains to intelligent tutoring systems. J. Bonar et al. in (\cite{ooa-tutor}), and D. R. Peachy~(\cite{plan-tutor}) both describe architectures of Intelligent Tutoring Systems with fixed data repositories. The system intelligence is derived in how it adapts to the learner. This limitation leads to ITS' being narrowly focused. The authors of (\cite{its-survey}) surveyed 68 existing intelligent tutoring systems found in literature over an 18 year period. The one challenge each system had to grapple with was how to accurately model the student's current understand of the material being taught. There were three overall strategies that evolved; (1) the overlay model tries to compare the student's behavior to an expert; (2) the perturbation model adds noise to account for possible misconceptions in understanding the domain; and (3) the learning base model focuses on the process of knowledge acquisition, because misconceptions are developed during that process. Both of the mentioned issues are related to the lack of ``common sense" or cognitive behavior. 

Cognitive or Intelligent Systems behavior is derived from both the knowledge-base and the system architecture. J. Laird, the creator of the SOAR architecture~(\cite{soar}) said ``if the architecture remains static then changing the knowledge will also change the behavior". SOAR is a general purpose architecture for developing intelligence systems; which is based on Production Systems. Production systems use ``If-Then" rules, shown in Figure~\ref{fig:rules-soar}a to define behavior, and typically have both a long-term and short-term or working memory. Figure~\ref{fig:rules-soar}b shows the Soar architecture. The long term memory can be further segmented into Procedural, Semantic, and Episodic regions also referred to as “Metacognition”. Each region holds different types of knowledge and representations allowing the intelligent agent to perform reasoning about the information in the Working Memory. 

\begin{figure}[h]
	\centering
    \includegraphics{images/rules-soar.png}
    \caption{(a) Example production rules; and (b) The SOAR Architecture as explained by J. Laird~(\cite{soar})}
    \label{fig:rules-soar}    
\end{figure}

\subsubsection{Working Memory - Frames}
Frames are another form of knowledge representation and serves as a basis for Common Sense Reasoning. What makes Frames so powerful is the ability to derive or infer meaning from natural language. In comparison with Production Rules which are a set of “if-then” patterns Frames are of a more detailed nature. Frames consists of fields referred to as “slots” and values called “fillers” which is not too different from an object in Object Oriented Programming, the slots and fillers representing the object’s state. In many cognitive architectures Production Rules and Frames work in tandem with each other with the former being used as long-term and the latter as short-term or working memory.

\subsubsection{Long Term Memory - Procedural, Semantic, Episodic}
\textbf{Procedural Memory:}
As discussed in the previous section the long term memory is composed of Procedural, Semantic, and Episodic knowledge representations. The Procedural representation take the form of Production Rules, a set of If-Then statements. Production Rules can be chained together to provide both deductive and inductive reasoning as shown in Figure~\ref{fig:rules-soar}a.

\textbf{Semantic Memory:}
Semantic Memory is one of two types of declarative or explicit memory which is made up of general knowledge. Knowledge in this segment is meant to be the accumulation of learned information, and therefore can be described with any type of knowledge representation. The Semantic Memory will be bootstrapped with functional, nonfunctional, and system requirements with domain knowledge.

\textbf{Episodic Memory:}
The other type of explicit memory is Episodic Memory encompasses the experience and specific events. Case Based Reasoning (CBR) is typically used to implement Episodic memory because of dealing with the experience aspect. Therefore as described by CBR the process of Retrieval, Adaptation, Evaluation, and Storage. This memory segment is the perfect for reusable data representations, and must incorporate a feedback learning loop.


\section{Related Work}
The architecture presented in (\cite{ooa-tutor}) closely resembles what is described in (\cite{ibm-auto-comp}) for autonomic computing with the obvious exception of the knowledge feedback loop. Autonomic computing draws inspiration from the Autonomic Nervous System (ANS) which is the system in the human body that controls the breathing, heartbeat, temperature regulation, and any other action that does not require conscience thought. Therefore, autonomic computing are software systems that are self-configuring, self-healing, and self-aware; systems of these types are often referred to as ``self-*" systems, where the ``*" is substituted for any learned behavior. The reference architecture for autonomic computing first introduced in~(\cite{ibm-auto-comp}) discuss the MAPE-K feedback loop referred to in Figure~\ref{fig:mape-k}. The MAPE-K loop is implemented by the ``Autonomic Manager" with manages the underlying system or artifact. In the `Monitor' phase, data is collected from sensors and evaluated by the manager and passed on to the `Analyze' phase. The task of the `Analyze' phase is to identify the current state of the system, which is then fed into the `Plan' phase. The goal of the `Plan' phase is too map a response that will drive the artifact towards the desired state. Specific effectors on the artifact are in charge of applying the computed response, thereby implementing the `Execute' phase of the MAPE-K loop. Finally, the `Knowledge' layer serves as an interface between the artifact and manager which, during regular operation and over time, acquires insights about the artifact. In general the research area of autonomic computing overlap with areas of meta-cognition and cognitive systems, the major similarity is the need for a knowledge-base and therefore, knowledge representation.

\begin{figure}[h]
	\centering
	\includegraphics[height=\maxheight, width=\maxwidth, trim={0 380 0 100}, clip]{images/mape-k.jpg}
	\caption{MAPE-K Feedback Loop.}
	\label{fig:mape-k}
\end{figure}

The research area of machine learning, more specifically, deep learning has a rich depth of literature using both `representational' \& `transfer' learning in a number of applications. The true strength of representational learning is that regardless of how it is obtained, once learned the knowledge can be reused~(\cite{represent-learning}); which leads directly into transfer learning. Transfer learning focuses on using pre-trained hidden layers to reduce the amount of time and data needed to train a Deep Neural Network (DNN)~(\cite{transfer-survey}). J. Lu et al. provided a framework to utilize previously-acquired knowledge to solve new but similar problems much more quickly and effectively~(\cite{transfer-int}). X. Jia et al.~(\cite{physics-ml}), for example, presented a number of ways physics could be used to constrain the learning of machine learning models. Typically there are three approaches to this method; (1) using physics models to simulate input data; (2) using physics models to generate initial weight values for neural networks; and (3) using physics models to regularize the learning function. Overall, this physics guided machine learning can be equated to transfer learning. Similarly, (\cite{meta-learn}), emulate properties of representation and transfer learning. The authors introduced a meta-learning method that could be combined with other model representations by reusing knowledge from previously learned tasks. R. M. Issaacson et al. described Metacognitive Knowledge Monitoring (MKM)as the ability to ``self-regulate" one's learning~(\cite{metacog}). Issaacson said that ``self-regulated learners are skillful at monitoring their learning and comprehension which as direct effect on each step in the self-regulation process." This cognitive behavior in students is exactly what the MAPE-K feedback loop in autonomic systems are designed to do; to be ``active, goal-directed" in achieving the specified learning task. The metaphorical threads of \textit{knowledge representation} and \textit{autonomic computing} weave together into a single idea, which is a ``general-purpose" machine learning model. 

Creating a general-purpose machine learning model is difficult. The difficulty lies in the fact that the learning objective is too broad, needing a high-order function to successfully train the model. Therefore, the higher the degree of the function, the more training data is required. Simply, the broader the learning objective, the more data that is needed. The majority of the research in the area of ``general-purpose machine learning" focuses on implementing ML as part of a larger framework or optimization~\cite{generic-obj-rec, gp-ml-framework, meta-opt}. However, the literature shows that there have been attempts to create such generic machine learning models (e.g.,~\cite{generic-tree-ensemble, comp-generic-ml, gml}) and, in each case, the authors are relying on ensemble methods, to create a general-purpose machine learning model or ``meta-model". Ensemble learners utilize the prediction of multiple machine learning model instances and combine them using a function~\cite{ensemble-methods}.

\begin{figure}[h]
	\centering
	\includegraphics[height=\maxheight, width=\maxwidth, trim={0 50 0 100}, clip]{images/its_model.png}
	\caption{Intelligent Tutoring Systems with Meta-Models}
	\label{fig:its-model}
\end{figure}

\section{Proposed Work}
The purpose of this research is to create a intelligent tutoring system with the ability to acquire new knowledge, and evaluate the effectiveness of that system. The plan is to modify a ITS built for network intrusion detection (\cite{id-its}), which is used to teach network administrators how to effectively detect and counter network intrusion. The goal is to transform the original architecture, specifically the Student, Tutoring, and Domain models into meta-models, shown in Figure~\ref{fig:its-model}. The decision to use a meta-models as surrogates was made in hopes of simplifying the resulting system, and removing some complexity. The ability of the meta-model to handle multi-modal data will be crucial, therefore, at two experiments are planned to test the validity of this approach, and adjustments could be made depending on the results. The experiments are as follows:
		
		\begin{enumerate}
  			\item Transfer knowledge learned on a similar computer system, but different network (this represents multi-domain data)
  			\item Transfer knowledge created from different modalities (this represents multi-modal data) 
		\end{enumerate}

Once the meta-model has been integrated, more experimentation will be required. However, due to the time constraints of the course, this may be a discussion for future work. This research will be quantitative in nature and use empirical analysis wherever possible.	The overall goal is to show that intelligent tutoring systems that both incorporate: (1) multi-modal \& multi-domain data; and (2) a learning feedback loop to acquire new knowledge are more effective than standard I

\section{References}
\printbibliography[heading=none]

\includepdf[pages=1,pagecommand={}]{TaskList.pdf}

\end{document}U